<?php

namespace App\Tests\Integration\Examples;

use App\Examples\DirectoryURLizer;
use PHPUnit_Framework_TestCase;

class PathURLizerTest extends PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        // SUT (system under test): what we are about to test.
        $sut = new DirectoryURLizer();

        // Expectation
        $this->assertEquals(
            [
                'http://local/file_1.txt',
                'http://local/file_2.txt',
            ],
            $sut->getUrls(__DIR__ . '/data', 'http://local/')
        );
    }
}
