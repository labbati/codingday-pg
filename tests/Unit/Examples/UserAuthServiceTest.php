<?php

namespace App\Tests\Unit\Examples;

use Mockery;
use App\Examples\Domain\User;
use App\Examples\Repository\UserRepository;
use App\Examples\UserAuthService;
use PHPUnit_Framework_TestCase;

class UserAuthServiceTest extends PHPUnit_Framework_TestCase
{
    public function test_authenticate_user_NOT_EXISTS()
    {
        // Preparing mocks
        $mockUserRepository = Mockery::mock(UserRepository::class);
        $mockUserRepository->shouldReceive('get')->with('user@gmail.com')->once()->andReturn(null);

        // SUT (system under test): what we are about to test.
        $sut = new UserAuthService($mockUserRepository);

        // Expectation
        $this->setExpectedException(\Exception::class, 'Invalid credentials.');

        // action
        $sut->authenticate('user@gmail.com', 'pasSw0rD');
    }

    public function test_authenticate_user_WRONG_PASSWORD()
    {
        // Preparing mocks
        $mockUser = Mockery::mock(User::class);
        $mockUser->shouldReceive('getPassword')->once()->andReturn('wrong_password');
        $mockUserRepository = Mockery::mock(UserRepository::class);
        $mockUserRepository->shouldReceive('get')->with('user@gmail.com')->once()->andReturn($mockUser);

        // SUT (system under test): what we are about to test.
        $sut = new UserAuthService($mockUserRepository);

        // Expectation
        $this->setExpectedException(\Exception::class, 'Invalid credentials.');

        // action
        $sut->authenticate('user@gmail.com', 'pasSw0rD');
    }
}
