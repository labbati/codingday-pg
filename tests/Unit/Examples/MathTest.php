<?php

namespace App\Tests\Unit\Examples;

use App\Examples\Math;
use PHPUnit_Framework_TestCase;

class MathTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        // SUT (system under test): what we are about to test.
        $sut = new Math();

        // Expectation
        $this->assertEquals(3, $sut->add(1, 2));
    }
}
