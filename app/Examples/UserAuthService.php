<?php

namespace App\Examples;

use App\Examples\Domain\User;
use App\Examples\Repository\UserRepository;

class UserAuthService
{
    /** @var UserRepository $userRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $email
     * @param string $password
     * @return User
     * @throws \Exception
     */
    public function authenticate($email ,$password)
    {
        /** @var User $user */
        $user = $this->userRepository->get($email);

        if (is_null($user) || $password !== $user->getPassword()) {
            throw new \Exception('Invalid credentials.');
        }

        if (!$user->isEnabled()) {
            throw new \Exception('User is disabled.');
        }

        return $user;
    }
}
