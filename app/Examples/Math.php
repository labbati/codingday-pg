<?php

namespace App\Examples;

/**
 * A utility class that performs basic operations on numbers
 */
class Math
{
    /**
     * Adds two integers
     *
     * @param int $num1
     * @param int $num2
     * @return int
     */
    public function add($num1, $num2)
    {
        return $num1 + $num2;
    }
}
