<?php

namespace App\Examples\Domain;

class User
{
    /** @var string $email */
    private $email;

    /** @var string $password */

    private $password;

    /** @var boolean $enabled */
    private $enabled;

    /**
     * User constructor.
     * @param string $email
     * @param string $password
     * @param bool $enabled
     */
    public function __construct($email, $password, $enabled)
    {
        $this->email = $email;
        $this->password = $password;
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
}
