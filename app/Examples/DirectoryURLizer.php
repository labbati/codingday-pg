<?php

namespace App\Examples;

/**
 * A utility class that creates URLs for all files in a folder.
 */
class DirectoryURLizer
{
    /**
     * Create absolute URLs for all files and dirs in a folder.
     * E.g.:
     *   # file_1.txt ====> http://www.example.com/file_1.txt
     *   # file_2.txt ====> http://www.example.com/file_2.txt
     *
     * @param string $dirPath
     * @param string $baseUrl
     * @return array
     */
    public function getUrls($dirPath, $baseUrl)
    {
        // Getting all files in $dirPath
        $allFiles = scandir($dirPath); // <<<=== File System Interaction
        $files = array_filter($allFiles, function($file) {
            return !in_array($file, ['.', '..']);
        });

        // Pre-pending base url to the file name
        array_walk($files, function (&$value) use ($baseUrl) {
            $value = $baseUrl . $value;
        });

        return array_values($files);
    }
}
