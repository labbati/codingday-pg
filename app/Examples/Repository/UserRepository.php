<?php

namespace App\Examples\Repository;

use App\Examples\Domain\User;

/**
 * Repository for entities of type User
 */
interface UserRepository
{
    /**
     * Retrieves a user by email or null if it does not exists.
     *
     * @param $email
     * @return User
     */
    public function get($email);
}
