# Coding Day 2016: Unit Testing

## Pre-requisites

In order to run tests for this project you need to set up both php and composer in your local environment.

1. *PHP 5.6+* - see php docs here: http://php.net/manual/en/install.php
2. *Composer* - follow installation instructions here: [https://getcomposer.org/doc/00-intro.md](https://getcomposer.org/doc/00-intro.md)
3. *Git \[optional\]* - download git from: [https://git-scm.com/downloads](https://git-scm.com/downloads) 

## Steps to run tests

1. Checkout the project running `git clone git@bitbucket.org:labbati/codingday-pg.git`
2. From the root of the project run `composer install`
3. In order to run Unit Tests: `./vendor/bin/phpunit tests/Unit`
4. In order to run Integration Tests: `./vendor/bin/phpunit tests/Integration`
5. In order to run Unit Tests WITH code coverage: `./vendor/bin/phpunit tests/Unit --coverage-html reports`, then you can open the file `./reports/index.html` using your browser of choice.

## Presentation

Presentation's slides can be found [here](https://docs.google.com/presentation/d/1xBPaenKS2bngO5nWjuJY2UuoXhhRLhdFHhhi8jEpG-0/edit) (italian)
